# Description

Test assignment for energy trading company

## Usage
* In VisualStudio: run ConsoleApp -> enter report start date and end date(included)

* In Terminal - go to ~\Intelometry\ConsoleApp and in command line run "run dotnet build" followed by "dotnet run"

## Testing
* In VisualStudio: run tests in CalculatorTests.cs

* In Terminal - go to ~\Intelometry\Tests and in command line run "dotnet test"