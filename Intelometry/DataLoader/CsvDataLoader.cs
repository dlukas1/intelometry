﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Domain;

namespace DataLoader
{
    public static class CsvDataLoader
    {
        public static List<DateProfile> LoadDateProfiles()
        {
            var fileContent = ReadFile("DateProfile.csv");

            var dateProfiles = fileContent.Select(s => new DateProfile
            {
                date_profile_id = int.Parse(s[0]),
                name = s[1],
                price = Convert.ToDouble(s[2], CultureInfo.InvariantCulture)
            }).ToList();

            return dateProfiles;
        }

        public static List<HoursData> LoadHoursData()
        {

            
            var fileContent = ReadFile("DateProfileHoursData.csv");

            var hoursData = fileContent.Select(s => new HoursData
            {
                id = int.Parse(s[0]),
                date_profile_id = int.Parse(s[1]),
                prevailing_start_date_time = Convert.ToDateTime(s[2], CultureInfo.InvariantCulture)
            }).ToList();

            return hoursData;
        }

        public static List<GenerationValue> LoadGenerationValues()
        {
            CultureInfo culture = new CultureInfo("en-US");
            culture.Calendar.TwoDigitYearMax = 2099;
            
            var fileContent = ReadFile("GenerationValues.csv");

            var generationValues = fileContent.Select(s => new GenerationValue
            {
                Date = Convert.ToDateTime(s[0], culture),
                HE1 = int.Parse(s[1]),
                HE2 = int.Parse(s[2]),
                HE3 = int.Parse(s[3]),
                HE4 = int.Parse(s[4]),
                HE5 = int.Parse(s[5]),
                HE6 = int.Parse(s[6]),
                HE7 = int.Parse(s[7]),
                HE8 = int.Parse(s[8]),
                HE9 = int.Parse(s[9]),
                HE10 = int.Parse(s[10]),
                HE11 = int.Parse(s[11]),
                HE12 = int.Parse(s[12]),
                HE13 = int.Parse(s[13]),
                HE14 = int.Parse(s[14]),
                HE15 = int.Parse(s[15]),
                HE16 = int.Parse(s[16]),
                HE17 = int.Parse(s[17]),
                HE18 = int.Parse(s[18]),
                HE19 = int.Parse(s[19]),
                HE20 = int.Parse(s[20]),
                HE21 = int.Parse(s[21]),
                HE22 = int.Parse(s[22]),
                HE23 = int.Parse(s[23]),
                HE24 = int.Parse(s[24]),
                HE25 = string.IsNullOrWhiteSpace(s[25]) ? 0 : int.Parse(s[25])
            }).ToList();

            return generationValues;
        }

        private static List<string[]> ReadFile(string fileName)
        {
            var filePath = @$"..\..\..\..\..\Test_task_2021\{fileName}";
            var filePathBackup = @$"..\..\Test_task_2021\{fileName}";
            List<string[]> fileContent;

            if (File.Exists(filePath))
            {
                fileContent = GetContent(filePath);
            }
            else if (File.Exists(filePathBackup))
            {
                fileContent = GetContent(filePathBackup);
            }
            else
            {
                Console.WriteLine($"File not found! Path = {filePath}");
                fileContent = new List<string[]>();
            }

            return fileContent;
        }

        private static List<string[]> GetContent(string filePath)
        {
            return File.ReadAllLines(filePath)
                .Skip(1)
                .Select(v => v.Split(','))
                .ToList();
        }
    }
}