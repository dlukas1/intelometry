﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;

namespace Calculator
{
    public class RevenueCalculator
    {
        private readonly List<DateProfile> _profiles;
        private readonly List<HoursData> _profilesHours;
        private readonly List<GenerationValue> _generationValues;

        public RevenueCalculator(List<DateProfile> profiles, List<HoursData> profilesHours, List<GenerationValue> generationValues)
        {
            _profiles = profiles;
            _profilesHours = profilesHours;
            _generationValues = generationValues;
        }


        public ReportDto GetReportForPeriod(DateTime start, DateTime end)
        {
            var report = new ReportDto {Start = start, End = end};
            foreach (var profile in _profiles)
            {
                report.ProfilesUsages.Add(profile, 0);
            }

            var defaultProfile = _profiles.Single(w => w.date_profile_id == 1);

            var calculatedDays = GetDays(start, end);

            foreach (var date in calculatedDays)
            {
                var generationValue = _generationValues.FirstOrDefault(w => w.Date.Year == date.Year && w.Date.Month == date.Month);

                if (generationValue == null) continue;

                var hourlyGenerations = generationValue.GetGeneratedValues();
                
                var profiledHoursByTime = _profilesHours
                    .Where(w => w.prevailing_start_date_time.Date == date)
                    .GroupBy(g => g.prevailing_start_date_time)
                    .ToDictionary(d => d.Key, d => d.ToList());

                // If generated value matches profiled hours - use matched profile, otherwise use default
                foreach (var generationData in hourlyGenerations)
                {
                    KeyValuePair<DateTime, List<HoursData>> hourDataList = profiledHoursByTime
                        .FirstOrDefault(w => w.Key.Hour == generationData.Key.Hour);

                    if (hourDataList.Key != default)
                    {
                        foreach (var hd in hourDataList.Value)
                        {
                            var profile = _profiles.FirstOrDefault(w => w.date_profile_id == hd.date_profile_id);
                            if (profile != null)
                            {
                                var generatedValue = generationValue.GetValueByHour(hd.prevailing_start_date_time.Hour);
                                report.ProfilesUsages[profile] += generatedValue;
                            }
                        }
                    }
                    else
                    {
                        report.ProfilesUsages[defaultProfile] += generationData.Value;
                    }
                }
            }
            
            return report;
        }

        private static List<DateTime> GetHours(DateTime date)
        {
            var startOfDay = StartOfDay(date);
            var endOfDay = EndOfDay(date);
            
            
            List<DateTime> hours = new List<DateTime>();
            while (startOfDay.AddHours(1) <= endOfDay)
            {
                startOfDay = startOfDay.AddHours(1);
                hours.Add(startOfDay);
            }
            return hours;
        }
        private static List<DateTime> GetDays(DateTime start, DateTime end)
        {
            List<DateTime> dates = new List<DateTime> {start};
            while (start.AddDays(1) <= end)
            {
                start = start.AddDays(1);
                dates.Add(start);
            }
            return dates;
        }

        private static DateTime EndOfDay(DateTime date)
        {
            return new(date.Year, date.Month, date.Day, 23, 59, 59, 999);
        }

        private static DateTime StartOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0);
        }
    }
}