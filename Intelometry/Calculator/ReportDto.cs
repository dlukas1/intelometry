﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain;

namespace Calculator
{
    public class ReportDto
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public Dictionary<DateProfile, int> ProfilesUsages { get; set; } = new();

        private int GeneratedValueTotal => ProfilesUsages.Sum(s => s.Value);

        private string ProfiledValues
        {
            get
            {
                var sb = new StringBuilder();
                foreach (var kvp in ProfilesUsages)
                {
                    sb.AppendLine($"Profile \"{kvp.Key.name}\" -> {kvp.Value} MW");
                }
                return sb.ToString();
            }
        }

        public double Revenue => ProfilesUsages.Sum(kvp => kvp.Value * kvp.Key.price);

        public override string ToString()
        {
            return @$"---Report period: {Start:d} - {End:d}---

Total generated {GeneratedValueTotal} MW
Total Revenue {Revenue} $

{ProfiledValues}";
        }
    }
}