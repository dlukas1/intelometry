﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Calculator;
using DataLoader;
using Domain;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime[] reportDates = null;
            while (reportDates == null )
            {
                reportDates = GetReportDates();
            }
            
            List<DateProfile> profiles = CsvDataLoader.LoadDateProfiles();
            List<HoursData> profilesHours = CsvDataLoader.LoadHoursData();
            List<GenerationValue> generationValues = CsvDataLoader.LoadGenerationValues();
            
            var calculator = new RevenueCalculator(profiles, profilesHours, generationValues);
            
            var res = calculator.GetReportForPeriod(reportDates[0], reportDates[1]);
            Console.WriteLine(res);
        }

        private static DateTime[] GetReportDates()
        {
            var culture = new CultureInfo("et-EE");
            const string errorMsg = "Invalid date format! Please use dd/MM/yyyy";
            
            Console.Write("Enter report start date (dd/MM/yyyy): ");
            if (!DateTime.TryParse(Console.ReadLine(), culture, DateTimeStyles.None, out var start))
            {
                Console.WriteLine(errorMsg);
                return null;
            }
            Console.Write("Enter report last date (dd/MM/yyyy): ");
            if (!DateTime.TryParse(Console.ReadLine(), culture, DateTimeStyles.None, out var end))
            {
                Console.WriteLine(errorMsg);
                return null;
            }
            return new[] {start, end};
        }
    }
}