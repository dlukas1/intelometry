using System;
using System.Collections.Generic;
using Calculator;
using DataLoader;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class CalculatorTests
    {
        private readonly List<DateProfile> _profiles = CsvDataLoader.LoadDateProfiles();
        private readonly List<HoursData> _profilesHours = CsvDataLoader.LoadHoursData();
        private readonly List<GenerationValue> _generationValues = CsvDataLoader.LoadGenerationValues();
        private RevenueCalculator _calculator;
        
        [TestMethod]
        public void WhenNoValuesGenerated_ReturnZeroRevenue()
        {
            // in Feb 2019 nothing was generated 1-Feb-19,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            // and DateProfileHoursData has entries , i.e. [927607128,105,2/11/2019 3:00], [927604166,105,2/11/2019 4:00] and [927572307,105,2/11/2019 5:00]
            _calculator = new RevenueCalculator(_profiles, _profilesHours, _generationValues);
            var res = _calculator.GetReportForPeriod(new DateTime(2019, 02,01), new DateTime(2019, 02,25));
            Assert.AreEqual(0, res.Revenue);
        }
        
        [TestMethod]
        public void WhenValuesGenerated_AndNoDateProfileHoursData_UseDefaultProfile()
        {
            // in May 2047 some energy is generated 1-May-47,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,
            // but no entry in DateProfileHoursData
            _calculator = new RevenueCalculator(_profiles, _profilesHours, _generationValues);
            var res = _calculator.GetReportForPeriod(new DateTime(2047, 05,01), new DateTime(2047, 05,01));
            Assert.AreEqual(16*1.75, res.Revenue);
        }
        [TestMethod]
        public void WhenEdgeValuesGenerated_AndNoDateProfileHoursData_UseDefaultProfile()
        {
            // Add row with edge cases (first and last hour) 1-Dec-18,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
            _generationValues.Add(new GenerationValue{Date = new DateTime(2018,12,1), HE1 = 1, HE24 = 1});
            // no entry in DateProfileHoursData for 2018
            // 3 days, 2 hours per day, default profile - should produce 3*2*1.75 = 10.5$
            _calculator = new RevenueCalculator(_profiles, _profilesHours, _generationValues);
            var res = _calculator.GetReportForPeriod(new DateTime(2018, 12,01), new DateTime(2018, 12,3));
            Assert.AreEqual(3*2*1.75, res.Revenue);
        }
        
        [TestMethod]
        public void SimpleTest_WithDummyData_ShouldReturnResult()
        {
            // add dummy data to for 4th of June 2036 DateProfileHoursData.csv:
                // 123,105,6/4/2036 20:00
                // 124,106,6/4/2036 20:00
                // 125,106,6/4/2036 21:00
            _profilesHours.AddRange(new List<HoursData>
            {
                new HoursData{id = 123, date_profile_id = 105, prevailing_start_date_time = DateTime.Parse("6/4/2036 20:00")},
                new HoursData{id = 124, date_profile_id = 106, prevailing_start_date_time = DateTime.Parse("6/4/2036 20:00")},
                new HoursData{id = 125, date_profile_id = 106, prevailing_start_date_time = DateTime.Parse("6/4/2036 21:00")}
            });
            // GenerationValues.csv has entry for June 2036: 1-Jun-36,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,
            // Expected 1 MW for profile "Off Peak(Price=1.5)", 2 MW for profile "On Peak(Price=2)" and 14 MW for profile "ATC(Price=1.75)",
            // total 1 MW * 1.5 $/MW + 2 MW * 2 $/MW + 14 MW * 1.75 $/MW = 1.5 $ + 4 $  + 24.5 = 30 $
            _calculator = new RevenueCalculator(_profiles, _profilesHours, _generationValues);
            var res = _calculator.GetReportForPeriod(new DateTime(2036, 06,04), new DateTime(2036, 06,04));
            Assert.AreEqual(30, res.Revenue);
        }
    }
}