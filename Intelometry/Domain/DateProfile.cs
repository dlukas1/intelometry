﻿namespace Domain
{
    public class DateProfile
    {
        // Auto generated with https://toolslick.com/generation/code/class-from-csv
        
        public int date_profile_id { get; set; }
        public string name { get; set; }
        public double price { get; set; }
    }
}