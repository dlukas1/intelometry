﻿using System;
using System.Collections.Generic;

namespace Domain
{
    // Auto generated with https://toolslick.com/generation/code/class-from-csv
    public class GenerationValue
    {
        public DateTime Date { get; set; }
        public int HE1 { get; set; }
        public int HE2 { get; set; }
        public int HE3 { get; set; }
        public int HE4 { get; set; }
        public int HE5 { get; set; }
        public int HE6 { get; set; }
        public int HE7 { get; set; }
        public int HE8 { get; set; }
        public int HE9 { get; set; }
        public int HE10 { get; set; }
        public int HE11 { get; set; }
        public int HE12 { get; set; }
        public int HE13 { get; set; }
        public int HE14 { get; set; }
        public int HE15 { get; set; }
        public int HE16 { get; set; }
        public int HE17 { get; set; }
        public int HE18 { get; set; }
        public int HE19 { get; set; }
        public int HE20 { get; set; }
        public int HE21 { get; set; }
        public int HE22 { get; set; }
        public int HE23 { get; set; }
        public int HE24 { get; set; }
        public int HE25 { get; set; }

        public Dictionary<DateTime, int> GetGeneratedValues()
        {
            var generations = new Dictionary<DateTime, int>();
            for (var i = 0; i < 24; i++)
            {
                var generatedPower = GetValueByHour(i);
                if (generatedPower != 0)
                {
                    generations.Add(new DateTime(Date.Year, Date.Month, Date.Day, i,0,0), generatedPower);
                }
            }

            return generations;
        }

        public int GetValueByHour(int hour)
        {
            return hour switch
            {
                0 => HE1,
                1 => HE2,
                2 => HE3,
                3 => HE4,
                4 => HE5,
                5 => HE6,
                6 => HE7,
                7 => HE8,
                8 => HE9,
                9 => HE10,
                10 => HE11,
                11 => HE12,  
                12 => HE13,
                13 => HE14,
                14 => HE15,
                15 => HE16,
                16 => HE17,
                17 => HE18,
                18 => HE19,
                19 => HE20,
                20 => HE21,
                21 => HE22,
                22 => HE23,
                23 => HE24,
                _ => 0
            };
        }
    }
}