﻿using System;

namespace Domain
{
    public class HoursData
    {
        // Auto generated with https://toolslick.com/generation/code/class-from-csv
        
        public int id { get; set; }
        public int date_profile_id { get; set; }
        public DateTime prevailing_start_date_time { get; set; } 
    }
}